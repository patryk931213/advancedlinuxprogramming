#!/bin/bash

TERMINAL=gnome-terminal

if [[ $# -eq 0 ]]; then
    echo "Please specify port number."
    exit 1
fi

#$TERMINAL -x sh -c "./test1.sh $1; bash" &
#$TERMINAL -x sh -c "./test2.sh $1; bash" &
#$TERMINAL -x sh -c "./test3.sh $1; bash" &
#$TERMINAL -x sh -c "./test4.sh $1; bash" &
#$TERMINAL -x sh -c "./fake_test.sh $1; bash" &
#$TERMINAL -x sh -c "./test1.sh $1; bash" &
#$TERMINAL -x sh -c "./test2.sh $1; bash" &
#$TERMINAL -x sh -c "./test3.sh $1; bash" &
#$TERMINAL -x sh -c "./test4.sh $1; bash" &
#$TERMINAL -x sh -c "./fake_test.sh $1; bash" &

for i in `seq 1 200`; do
./test1.sh $1 >> t1.txt &
./test2.sh $1 >> t2.txt &
./test3.sh $1 >> t3.txt &
./test4.sh $1 >> t4.txt &
./fake_test.sh $1 >> f.txt &
done
