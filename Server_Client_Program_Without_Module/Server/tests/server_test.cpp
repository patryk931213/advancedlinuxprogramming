extern "C" {
	#include "../src/os.h"
	#include "../src/network_fun.h"
	#include "../src/server.h"
	#include "../src/list.h"
}
#include "os_mock.h"
#include <sys/ioctl.h>

#include <gtest/gtest.h>

using namespace ::testing;

TEST(send_msg, cannot_write_length)
{
	int fd=1;
	size_t len = 1;
	size_t size_len = 4;
	os_len_write_mock olwm;
	EXPECT_FUNCTION_CALL(olwm,(fd,len,size_len)).WillOnce(Return(1));
	
	ASSERT_EQ(send_msg(fd,"A",1), 1);
}
TEST(send_msg, msg_null)
{
	int fd=1;
	char *msg=NULL;
	
	ASSERT_EQ(send_msg(fd,msg,0), 1);
}
TEST(read_configuration, msg_null)
{
		
	read_configuration();
}

TEST(get_interfaces_list, bad_index)
{
	int fd = 1;
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = 1;
	
	os_ioctl_mock oim;
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFNAME, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(-1)));
	
	ASSERT_EQ(get_interfaces_list(fd), 0);
}

TEST(if_interface_exist, interface_doesnt_exist)
{
	char *iface = "ethx";
	int fd = 1;
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = 1;
	
	os_ioctl_mock oim;
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFNAME, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(-1)));
	
	ASSERT_EQ(if_interface_exist(fd, iface), 1);
}

TEST(if_status, status_down)
{
	char *iface = "eth0";
	int fd = 1;
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	os_ioctl_mock oim;
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFFLAGS, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(0)));
	
	ASSERT_EQ(if_status(fd, iface), 1);
}

TEST(set_ip_address_and_net_mask, cannot_set_netmask)
{
	int fd = 1;
	char *iface = "eth0";
	char *ip_addr = "192.168.1.1";
	char *netmask = "255.0";
	
	struct sockaddr_in ip;
	struct ifreq ifr;
	
	os_ioctl_mock oim;
	
	inet_pton(AF_INET, ip_addr, &ip.sin_addr);
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr = ip.sin_addr;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCSIFADDR, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(0)));
	
	inet_pton(AF_INET, netmask, &ip.sin_addr);
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr = ip.sin_addr;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCSIFNETMASK, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(-1)));
	
	ASSERT_EQ(set_ip_address_and_netmask(fd, iface, ip_addr, netmask), 1);
	
}

TEST(set_mac_address, cannot_mac_address)
{
	int fd = 1;
	char *iface = "eth0";
	char *mac_char = "12:23:12";
	
	struct ifreq ifr;
	struct sockaddr hwaddr = {};
	
	os_ioctl_mock oim;
	
	sscanf(mac_char, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &hwaddr.sa_data[0], &hwaddr.sa_data[1], &hwaddr.sa_data[2],
		   		&hwaddr.sa_data[3], &hwaddr.sa_data[4], &hwaddr.sa_data[5]);
	hwaddr.sa_family = ARPHRD_ETHER;
	
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strcpy(ifr.ifr_name, iface);
	ifr.ifr_hwaddr = hwaddr;
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCSIFHWADDR, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(-1)));
	
	ASSERT_EQ(set_mac_address(fd, iface, mac_char), 1);
	
}

TEST(get_up_down_status, ioctl_error_status_down)
{
	int fd = 1;
	char *iface = "ethx";
	struct ifreq ifr;
		
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	os_ioctl_mock oim;
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFFLAGS, _)).WillOnce(Return(0));
	
	EXPECT_NE(ifr.ifr_flags, IFF_UP);
	
	EXPECT_TRUE( 0 == memcmp( get_up_down_status(fd, iface), " status - down\n", 15 ) );
}

TEST(get_mac_address, ioctl_error_status_down)
{
	int fd = 3;
	int i = 0;
	char *iface = "eth0";
	struct ifreq ifr;
	struct sockaddr hwaddr;
	char message[100];
	char buff[5];
	strcpy(message, iface);
	char *mac_char = "12:12:32:32:12:32";
	strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);
	
	os_ioctl_mock oim;
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFHWADDR, _)).WillOnce(Return(0));
	
	strcat(message, " MAC address - ");
	
	sscanf(mac_char, "%02x:%02x:%02x:%02x:%02x:%02x", &hwaddr.sa_data[0], &hwaddr.sa_data[1], &hwaddr.sa_data[2],
			   		&hwaddr.sa_data[3], &hwaddr.sa_data[4], &hwaddr.sa_data[5]);
	
	for (i = 0; i < 6; ++i)
	{
		sprintf(buff, "%02x", (unsigned char) hwaddr.sa_data[i]);
		strcat(message, buff);
		if(i < 5)
		{
			strcat(message, ":");
		}
		buff[0] = 0;
	}
	strcat(message, "\n");
	
	EXPECT_FALSE( 1 == memcmp( get_mac_address(iface), "eth0 MAC address - 12:12:32:32:12:32\n", sizeof(get_mac_address(iface)) ) );
}

TEST(get_interfaces_details, cannot_write_length)
{
	int fd=1;
	char *msg = "2-";
	int i, j, k, count = 0;
	for (k = 0; k < strlen(msg); k++)
	{
		if(msg[k] == '-')
		{
			count++;
		}
	}
	char message[10000];
	EXPECT_EQ(1,count);
	
	size_t len = strlen(message);
	size_t size_len = sizeof(size_t);
	os_len_write_mock olwm;
	os_write_mock owm;
	EXPECT_FUNCTION_CALL(olwm,(fd,len,size_len)).WillOnce(Return(1));
		
	ASSERT_EQ(get_interfaces_details(fd,message), 1);
}

TEST(get_interfaces_details, get_ipv6_fail)
{
	int fd=1;
	char *msg = "2-eth0-d";
	int i, j, k, count = 0;
	for (k = 0; k < strlen(msg); k++)
	{
		if(msg[k] == '-')
		{
			count++;
		}
	}
	char *interfaces[count + 1];
	char message[10000];
	interfaces[0] = "2";
	interfaces[1] = "-";
	interfaces[2] = "eth0";
	interfaces[3] = "-";
	interfaces[4] = "d";
	EXPECT_EQ(2,count);
	EXPECT_EQ(0, strcmp("d", interfaces[4]));
	EXPECT_EQ(1, if_interface_exist(fd, interfaces[2]));

	struct ifaddrs *ifa = NULL, *entry_ptr = NULL;
	void *addr_ptrs = NULL;
	int rc = 0;
	char *iface="eth0";
	char *mess = (char*)malloc(1000 * sizeof(char));
	strcpy(mess, iface);
	char buff[INET6_ADDRSTRLEN];
	rc = getifaddrs(&ifa);	

	//os_socket_mock osm;
	//os_ioctl_mock oim;

	//EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(osm,(_,_,_)).WillOnce(Return(fd));

	ASSERT_EQ(*get_ipv6(iface),*mess);
}

TEST(get_ipv4, get_ipv4_fail)
{

	int fd=1;
	char *iface="eth0";
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, "");
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strcat(message, " Interface is down \n");	

	os_socket_mock osm;
	os_ioctl_mock oim;

	EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(osm,(_,_,_)).WillOnce(Return(fd));

	ASSERT_EQ(*get_ipv4_and_netmask(fd,iface),*message);

}
TEST(get_ipv6, get_ipv6_fail)
{

	struct ifaddrs *ifa = NULL, *entry_ptr = NULL;
	void *addr_ptrs = NULL;
	int rc = 0;
	char *iface="eth0";
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, iface);
	char buff[INET6_ADDRSTRLEN];
	rc = getifaddrs(&ifa);	

	//os_socket_mock osm;
	//os_ioctl_mock oim;

	//EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(osm,(_,_,_)).WillOnce(Return(fd));

	ASSERT_EQ(*get_ipv6(iface),*message);

}

TEST(accpet, accept1)
{
	int fd=1;
	struct sockaddr *adr;
	socklen_t *addrlen;
		
	ASSERT_EQ(os_accept(fd,adr,addrlen),-1);
}

TEST(listen, listen1)
{
	int fd=1,backlog=2;
	struct sockaddr *adr;
	socklen_t *addrlen;
		
	ASSERT_EQ(os_listen(fd,backlog),-1);
}
TEST(read, read1)
{
	int fd=1;
	char *buff;
	size_t count;
		
	ASSERT_EQ(os_read(fd,buff,count),-1);
}
TEST(lenread, lenread1)
{
	int fd=1;
	size_t *buff;
	size_t count;
		
	ASSERT_EQ(os_len_read(fd,buff,count),-1);
}
TEST(bind, bind1)
{
	int fd=1;
	const struct sockaddr *adr;
	socklen_t addrlen;
		
	ASSERT_EQ(os_bind(fd,adr,addrlen),-1);
}


TEST(get_interfaces_details, get_up_down)
{
	int fd=1;
	char *msg = "2-eth0-a";
	int i, j, k, count = 0;
	for (k = 0; k < strlen(msg); k++)
	{
		if(msg[k] == '-')
		{
			count++;
		}
	}
	char message[10000];
	EXPECT_EQ(2,count);

	char *iface = "eth0";
	struct ifreq ifr;
		
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	os_ioctl_mock oim;
	
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFFLAGS, _)).WillOnce(Return(0));
	
	EXPECT_NE(ifr.ifr_flags, IFF_UP);
	
	EXPECT_TRUE( 0 == memcmp( get_up_down_status(fd, iface), " status - down\n", 15 ) );
	
	size_t len = strlen(message);
	size_t size_len = sizeof(size_t);
	os_len_write_mock olwm;
	os_write_mock owm;
	EXPECT_FUNCTION_CALL(olwm,(fd,len,size_len)).WillOnce(Return(1));
		
	ASSERT_EQ(get_interfaces_details(fd,message), 1);
}

TEST(set_interfaces_details, interface_doesnt_exist)
{
	char *iface = "ethx";
	char message[100];
	strcpy(message,"");
	int fd = 1;
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = 1;
	char *expect = "Interface ethx doesn't exist";
	
	os_ioctl_mock oim;
	EXPECT_FUNCTION_CALL(oim,(fd, SIOCGIFNAME, _)).WillOnce(DoAll(MemCmpArg<2>(&ifr, sizeof(struct ifreq)), Return(-1)));
	
	ASSERT_EQ(if_interface_exist(fd, iface), 1);
	
	strcat(message, "Interface ");
	strcat(message, iface);
	strcat(message, " doesn't exist");
	
	EXPECT_TRUE( 0 == memcmp( expect, message, sizeof(expect) ) );
}

TEST(init_server, cannot_create_socket)
{
	int port = 1,cl_count=2;
	pthread_t t1;

	os_socket_mock osm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}
TEST(init_server, cannot_bind_socket)
{
	int port = 1,cl_count=2,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}
TEST(init_server, cannot_close_socket)
{
	int port = 1,cl_count=2,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(srv_fd)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}
TEST(init_server, cannot_listen)
{
	int port = 1,cl_count=1,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}

TEST(init_server, cannot_create_epoll)
{
	int port = 1,cl_count=1,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_epoll_create_mock oecm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(cl_count)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}

TEST(init_server, cannot_close_after_create_epoll)
{
	int port = 1,cl_count=1,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_epoll_create_mock oecm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(cl_count)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(srv_fd)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}

TEST(init_server, cannot_close_after_listen)
{
	int port = 1,cl_count=1,srv_fd=3;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(srv_fd)).WillOnce(Return(-1));
		
	ASSERT_EQ(run_server(port,cl_count,t1), 1);
}
TEST(init_server, cannot_add_server_socket_to_epoll)
{
	int port = 1,cl_count=1,srv_fd=3,epoll_fd=4;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(cl_count)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillOnce(Return(0)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(2).WillRepeatedly(Return(0));

	ASSERT_EQ(run_server(port,cl_count,t1),1);
}

TEST(init_server, cannot_close_after_add_server_socket_to_epoll)
{
	int port = 1,cl_count=1,srv_fd=3,epoll_fd=4;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(cl_count)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillOnce(Return(0)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(2);

	ASSERT_EQ(run_server(port,cl_count,t1),1);
}

TEST(init_server, init_passed_ok_server_cannot_wait_for_events)
{
	int port = 1,cl_count=1,srv_fd=3,epoll_fd=4;
	pthread_t t1;

	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(2).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
}

/*TEST(run_server, cannot_wait_for_events_and_cannot_close)
{
	int port=1,cl_count=1,srv_fd=3,epoll_fd=5;
	pthread_t t1;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(2);
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}*/

TEST(run_server, accept_one_cannot_wait_for_events)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = srv_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oam,(srv_fd,_,_)).WillOnce(Return(cli_fd));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}
TEST(run_server, accept_one_cannot_wait_for_events_one_close)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = srv_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(1)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oam,(srv_fd,_,_)).WillOnce(Return(cli_fd));
	EXPECT_FUNCTION_CALL(ocm,(_)).WillOnce(Return(-1));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

/*TEST(run_server, accept_one_cannot_wait_for_events_os_close)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = srv_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(1)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oam,(srv_fd,_,_)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3);
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}*/

TEST(run_server, accept_one_and_cannot_read_len)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = cli_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_can_read_len_cannot_read_message)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = cli_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_read_mock orm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(4));
	EXPECT_FUNCTION_CALL(orm,(_,_,_)).WillOnce(Return(-1));	
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_can_read_len_and_message)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = cli_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_read_mock orm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(orm,(_,_,_)).WillOnce(Return(0));	
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_cannot_get_message)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = cli_fd;
	char *msg=NULL;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_read_mock orm;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(orm,(_,_,_)).WillOnce(Return(0));	
	//ASSERT_EQ(get_message(cli_fd,msg),1);
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, cannot_accept_client_cannot_wait_for_events)
{
	int port=1,cl_count=1,srv_fd=3,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = srv_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oam,(srv_fd,_,_)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, cannot_accept_client_cannot_close_fd_cannot_wait_for_events)
{
	int port=1,cl_count=1,srv_fd=3,epoll_fd=5;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	e.data.fd = srv_fd;

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(2).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oam,(srv_fd,_,_)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_get_interfaces_list)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5,len=2;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	char *buff=0;
	e.data.fd = cli_fd;
	struct ifreq ifr = {};
	int i = 1;
	//buff[len] = 0;
	//char *message = (char*)malloc(1000 * sizeof(char));
	//strcpy(message, "");
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = i;
	const char *msg="1-eth0";

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_len_write_mock oslwm;
	os_read_mock orm;
	os_ioctl_mock oim;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(2)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)))\
					.WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(orm,(_,_,_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(oslwm,(_,_,_)).WillOnce(Return(0));	
	//EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(0));
	//get_message(cli_fd,msg);
	EXPECT_FUNCTION_CALL(ocm,(_)).Times(3).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_get_interfaces_list2)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5,len=2;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	char *buff=0;
	e.data.fd = cli_fd;
	struct ifreq ifr = {};
	int i = 1;
	//buff[len] = 0;
	//char *message = (char*)malloc(1000 * sizeof(char));
	//strcpy(message, "");
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = i;
	const char *msg="1-eth0";

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_len_write_mock oslwm;
	os_read_mock orm;
	os_ioctl_mock oim;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillRepeatedly(Return(0));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(1)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(ocm,(_)).WillOnce(Return(-1));
	//EXPECT_FUNCTION_CALL(oslwm,(_,_,_)).WillOnce(Return(0));	
	//EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(0));
	//get_message(cli_fd,msg);
	//EXPECT_FUNCTION_CALL(ocm,(_)).Times(2).WillRepeatedly(Return(0));
	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, accept_one_and_get_interfaces_list3)
{
	int port=1,cl_count=1,srv_fd=3,cli_fd=4,epoll_fd=5,len=2;
	pthread_t t1;
	struct epoll_event e;
	e.events = EPOLLIN;
	char *buff=0;
	e.data.fd = cli_fd;
	struct ifreq ifr = {};
	int i = 1;
	ifr.ifr_addr.sa_family = AF_INET;
	ifr.ifr_ifindex = i;
	const char *msg="1-eth0";

	os_epoll_wait_mock oewm;
	os_epoll_create_mock oecm;
	os_epoll_ctl_mock oectlm;
	os_socket_mock osm;
	os_bind_mock obm;
	os_listen_mock olm;
	os_close_mock ocm;
	os_accept_mock oam;
	os_len_read_mock oslrm;
	os_len_write_mock oslwm;
	os_read_mock orm;
	os_ioctl_mock oim;

	EXPECT_FUNCTION_CALL(osm,(_, _,0)).WillOnce(Return(srv_fd));
	EXPECT_FUNCTION_CALL(obm,(srv_fd, _,_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(olm,(srv_fd,cl_count)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oecm,(_)).WillOnce(Return(epoll_fd));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).Times(3).WillOnce(Return(0)).WillOnce(Return(0)).WillOnce(Return(-1));
	EXPECT_FUNCTION_CALL(oewm,(epoll_fd,_,cl_count,-1))\
					.Times(1)\
					.WillOnce(DoAll(SetArgPointee<1>(e), Return(1)));
	EXPECT_FUNCTION_CALL(oslrm,(_,_,_)).WillOnce(Return(-1));	
	ASSERT_EQ(run_server(port,cl_count,t1),1);
	
	
}

TEST(run_server, get_mesage_fail)
{
	int fd=1;
	char *msg=NULL;
	
	ASSERT_EQ(get_message(fd,msg),1);
	
}
TEST(run_server, get_mesage_case_1)
{
	int fd=1;
	char *msg="1";
	os_len_write_mock olwm;
	
	EXPECT_FUNCTION_CALL(olwm,(_,_,_)).WillOnce(Return(1));

	ASSERT_EQ(get_message(fd,msg),1);
	
}
TEST(run_server, get_mesage_case_1_fail_on_write)
{
	int fd=1,len=1;
	char *msg="1";
	os_len_write_mock olwm;
	os_write_mock osw;
	
	EXPECT_FUNCTION_CALL(olwm,(_,_,_)).WillOnce(Return(sizeof(size_t)));
	EXPECT_FUNCTION_CALL(osw,(_,_,_)).WillOnce(Return(len));

	ASSERT_EQ(get_message(fd,msg),1);
	
}
/*TEST(run_server, remove_client)
{
	int fd=1,len=1;
	char *msg="1";
	os_close_mock osm;
	os_epoll_ctl_mock oectlm;
	
	EXPECT_FUNCTION_CALL(osm,(_)).WillOnce(Return(0));
	EXPECT_FUNCTION_CALL(oectlm,(_,_,_,_)).WillOnce(Return(0));

	remove_client(fd);
	
}*/
/*TEST(run_server, get_mesage_case_2_interface)
{
	int fd=1,len=1;
	char *msg="3-eth0";
	int i = 1;	
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	os_len_write_mock olwm;
	os_write_mock osw;
	os_ioctl_mock oim;
	
	EXPECT_FUNCTION_CALL(oim,(_,_,_)).WillOnce(Return(-1));

	ASSERT_EQ(get_message(fd,msg),1);
	
}*/

