if [[ "$(id -u)" -ne 0 ]]; then
    echo "You must run this script as root"
    exit 1
fi

export LD_LIBRARY_PATH=/usr/local/lib
./server
