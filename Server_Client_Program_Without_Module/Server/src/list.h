#include <malloc.h>
#include <sys/epoll.h>

typedef struct node
{
	struct node *next;
	struct node *prev;
	int key;
}node;

node *initiation(node *head);
node *add_element(node *head, int key);
node *search_element(node *head, int key);
node *delete_element(node *head, int key);
void delete_descriptors(node *head, struct epoll_event *ev);
void delete_list(node *head);
