#include "server.h"
#include "network_fun.h"
#include <pthread.h>

int main(int argc, char *argv[])
{
	int *args = read_configuration();
	pthread_t t1;

	printf("Port: %d\n", args[0]);
	printf("Number of clients: %d\n", args[1]);

	pthread_create(&t1, NULL, input_stdin, NULL);
	
	if (run_server(args[0], args[1], t1) == 1)
	{
		pthread_join(t1, NULL);
		return 0;
	}

	return 0;
}
