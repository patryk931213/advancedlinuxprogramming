#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libconfig.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netpacket/packet.h>
#include <netinet/if_ether.h>
#include <linux/sockios.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <net/route.h>
#include <fcntl.h>
#include <malloc.h>
#include <ifaddrs.h>
#include <regex.h>

int send_msg(int fd, const char* msg, size_t len);
int *read_configuration();
int get_interfaces_list(int fd);
int get_interfaces_details(int fd, char *msg);
char *get_ipv4_and_netmask(int sock_fd, char *iface);
char *get_up_down_status(int fd, char iface[]);
char *get_mac_address(char iface[]);
char *get_ipv6(char iface[]);
int set_ip_address_and_netmask(int fd, char *iface_name, char *ip_addr, char *gateway_addr);
int set_interfaces_details(int fd, char *msg);
int set_mac_address(int fd, char *iface, char mac_char[]);
int if_interface_exist(int fd, char iface[]);
int if_status(int fd, char iface[]);
int another_option(int fd);