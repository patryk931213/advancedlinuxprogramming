#include "list.h"
#include "server.h"

//function using to initiation list

node* initiation(node* head)
{
	head = NULL;
	return head;
}

//function using to add element to list

node *add_element(node *head, int key)
{
	node *tail, *before, *new_node, *tmp;
	tail = head;
	if (head != NULL)
	{
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
	}
	new_node = malloc(sizeof(node));
	new_node->next = NULL;
	new_node->key = key;
	if (head == NULL)
	{
		head = new_node;
		head->next = NULL;
		head->prev = NULL;
	}
	else
	{
		if (tail == head)
		{
			if (search_element(head, new_node->key) != NULL)
			{
				return head;
			}
			if (head->key > new_node->key)
			{
				tmp = head;
				head = new_node;
				head->next = tmp;
				head->prev = NULL;
				tmp->next = NULL;
				tmp->prev = head;
			}
			else if (head->key < new_node->key)
			{
				head->next = new_node;
				new_node->next = NULL;
				new_node->prev = head;
			}
		}
		else if (head->key == key)
		{
			printf("Key is in list!\n");
		}
		else if (tail != head)
		{
			before = head;
			while (before->next != tail)
			{
				before = before->next;
			}
			if (before == head)
			{
				if (search_element(head, new_node->key) != NULL)
				{
					return head;
				}
				else if (new_node->key < head->key)
				{
					tmp = head;
					new_node->next = head;
					new_node->prev = NULL;
					head = new_node;
					tmp->prev = head;
				}
				else if (new_node->key > head->key && new_node->key < tail->key)
				{
					head->next = new_node;
					new_node->next = tail;
					new_node->prev = head;
					tail->next = NULL;
					tail->prev = new_node;
				}
				else if (tail->key < new_node->key)
				{
					tail->next = new_node;
					new_node->next = NULL;
					new_node->prev = tail;
				}
			}
			else if (before != head)
			{
				if (search_element(head, new_node->key) != NULL)
				{
					return head;
				}
				else if (new_node->key < head->key)
				{
					tmp = head;
					new_node->next = head;
					head = new_node;
					tmp->prev = head;
					head->prev = NULL;
				}
				else if (new_node->key > tail->key)
				{
					tail->next = new_node;
					new_node->prev = tail;
					new_node->next = NULL;
				}
				else if (new_node->key > head->key && new_node->key < tail->key)
				{
					node *predecessor, *successor;
					successor = head;
					while (new_node->key > successor->key && successor->next != NULL)
					{
						successor = successor->next;
					}
					predecessor = head;
					while (predecessor->next != successor && predecessor->next != NULL)
					{
						predecessor = predecessor->next;
					}
					predecessor->next = new_node;
					new_node->prev = predecessor;
					new_node->next = successor;
					successor->prev = new_node;
				}
			}
		}
	}
	return head;
}

//function using to search element in list

node *search_element(node *head, int key)
{
	node *current, *node;
	current = head;
	node = NULL;
	while (current->next != NULL)
	{
		if (current->key != key)
		{
			current = current->next;
		}
		else if (current->key == key)
		{
			return current;
		}
	}
	if (current->next == NULL && current->key == key)
	{
		node = current;
		return node;
	}
	if (current->next == NULL && current->key != key)
	{
		return NULL;
	}
	return NULL;
}

//function using to delete element from list

node *delete_element(node *head, int key)
{
	node *predecessor, *removed, *successor, *tmp;
	predecessor = head;
	tmp = head;
	while(tmp->key != key)
	{
		tmp = tmp->next;
	}
	removed = tmp;
	if (head == NULL)
	{
		printf("Key is not on list!\n\n");
	}
	else
	{
		while (predecessor->next != removed)
		{
			predecessor = predecessor->next;
		}
		if (removed->next != NULL)
		{
			successor = removed->next;
		}
		if (removed->next == NULL)
		{
			if (removed == head)
			{
				free(removed);
				removed = NULL;
				head = NULL;
			}
			predecessor->next = NULL;
			free(removed);
			removed = NULL;
		}
		else if (removed == head)
		{
			head = removed->next;
			head->prev = NULL;
			free(removed);
			removed = NULL;
		}
		else if (removed != head && removed->next != NULL)
		{
			predecessor->next = removed->next;
			successor->prev = predecessor;
			free(removed);
			removed = NULL;
		}		
	}
	return head;
}

//function using to delete descriptors from server

void delete_descriptors(node *head, struct epoll_event *ev)
{
	node *tmp;
	for (tmp = head; tmp != NULL; tmp = tmp->next)
	{
		if(tmp->key == ev->data.fd)
		{
			remove_client(ev->data.fd);
		}
	}
	free(tmp);
}

//function using to delete list

void delete_list(node *head)
{
	node *tmp = head;
	while(tmp)
	{
		head = head->next;
		free(tmp);
		tmp = head;
	}
	head = NULL;
}
