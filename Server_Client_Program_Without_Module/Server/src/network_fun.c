#include "network_fun.h"
#include "os.h"

//function using to send message to client

int send_msg(int fd, const char* msg, size_t len)
{
	
	if(msg == NULL)
	{
		printf("\nERROR: Message is empty\n");
		return 1;
	}
	
	//printf("\n\nSend to client (fd - %d):\n\n%s", fd, msg);

	if (os_len_write(fd, len, sizeof(size_t)) != sizeof(size_t)) 
	{
		printf("\nERROR: Cannot write length\n");
		return 1;
	}

	if (os_write(fd, msg, len) != len) 
	{
		printf("\nERROR: Cannot write message\n");
		return 1;
	}
	
	return 0;
}

//function using to read libconfig configuration

int *read_configuration()
{
	config_t cfg;
	config_setting_t *setting;
	int port, clients;
	static int args[2];
	 
	char *config_file_name = "config.txt";
	 
	config_init(&cfg);
	 
	if (!config_read_file(&cfg, config_file_name))
	{
		printf("\nERROR: %s:%d - %s", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
	}

	setting = config_lookup(&cfg, "configuration");
	
	if (setting != NULL)
	{
		if (config_setting_lookup_int(setting, "port", &port))
		{
			args[0] = port;
		}
		else
		{
			printf("\nERROR: No 'port' setting in configuration file.");
		}
	 
		if (config_setting_lookup_int(setting, "clients", &clients))
		{
			args[1] = clients;
		}
		else
		{
			printf("\nERROR: No 'clients' setting in configuration file.");
		}
	 
		printf("\n");
	}
	
	config_destroy(&cfg);
	return args;
}

//function using to get network interfaces list

int get_interfaces_list(int fd)
{
	int i = 1;
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, "");
		
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	for (; ; ++i) 
	{
		ifr.ifr_ifindex = i;
	    if(os_ioctl(fd, SIOCGIFNAME, &ifr) == -1)
	    {
	    	break;
	    }
	    else
	    {
	    	strncat(message, ifr.ifr_name, sizeof(ifr.ifr_name));
	    	strcat(message, "\n");
	    }
	}
	if (send_msg(fd, message, strlen(message)) != 0)
	{
		printf("\nERROR: Cannot send message\n");
		free(message);
		return 1;
	}

	free(message);
	
	return 0;
}

//function using to get network interfaces details

int get_interfaces_details(int fd, char *msg)
{
	if(msg == NULL)
	{
		printf("\nERROR: Cannot get message\n");
		return 1;
	}
	
	int i, j, k, count = 0;
	for (k = 0; k < strlen(msg); k++)
	{
		if(msg[k] == '-')
		{
			count++;
		}
	}
	
	char *interfaces[count + 1];
	char message[10000];
	interfaces[0] = strtok (msg, "-");
	strcpy(message,"");
	
	if(count == 1)
	{
		strcat(message, "No behaviour for this message.\n");
	}
	else
	{
		for (i = 1; i < count + 1; i++)
		{
			interfaces[i] = strtok (NULL, "-");
		}
		for (j = 2; j < count + 1; j += 2)
		{
			if (strcmp(interfaces[j], "a") == 0)
			{
				if(if_interface_exist(fd, interfaces[j - 1]) == 0)
				{
					strcat(message, interfaces[j - 1]);
					char *tmp = get_up_down_status(fd, interfaces[j - 1]);
					strcat(message, tmp);
					free(tmp);
				}
				else
				{
					strcat(message, "Interface ");
					strcat(message, interfaces[j - 1]);
					strcat(message, " doesn't exist, cannot return up/down status\n");
				}
			}
			else if (strcmp(interfaces[j], "b") == 0)
			{
				if(if_interface_exist(fd, interfaces[j - 1]) == 0)
				{
					char *tmp = get_mac_address(interfaces[j - 1]);
					strcat(message, tmp);
					free(tmp);
				}
				else
				{
					strcat(message, "Interface ");
					strcat(message, interfaces[j - 1]);
					strcat(message, " doesn't exist, cannot return MAC address\n");
				}
			}
			else if (strcmp(interfaces[j], "c") == 0)
			{
				if(if_interface_exist(fd, interfaces[j - 1]) == 0)
				{
					strcat(message, interfaces[j - 1]);
					char *tmp = get_ipv4_and_netmask(fd, interfaces[j - 1]);
					strcat(message, tmp);
					free(tmp);
				}
				else
				{
					strcat(message, "Interface ");
					strcat(message, interfaces[j - 1]);
					strcat(message, " doesn't exist, cannot return IPv4 address and netmask\n");
				}
			}
			else if (strcmp(interfaces[j], "d") == 0)
			{
				if(if_interface_exist(fd, interfaces[j - 1]) == 0)
				{
					char *tmp = get_ipv6(interfaces[j - 1]);
					strcat(message, tmp);
					free(tmp);
				}
				else
				{
					strcat(message, "Interface ");
					strcat(message, interfaces[j - 1]);
					strcat(message, " doesn't exist, cannot return IPv6 address\n");
				}			
			}
			else
			{
				strcat(message, "No behaviour for this message.\n");
			}
		}
	}
	if (send_msg(fd, message, strlen(message)) != 0)
	{
		printf("\nERROR: Cannot send message\n");
		return 1;
	}
	
	return 0;
}

//function using to get network interfaces IPv4 address and netmask

char *get_ipv4_and_netmask(int sock_fd, char iface[])
{
	int fd;
	struct ifreq ifr;
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, "");
	
	if(if_status(sock_fd, iface) == 0)
	{
		fd = socket(AF_INET, SOCK_DGRAM, 0);
		
		if (fd == -1)
		{
			printf("\nERROR: Cannot create socket\n");
		}
			 
		ifr.ifr_addr.sa_family = AF_INET;
			 
		strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);
			     
		if (os_ioctl(fd, SIOCGIFADDR, &ifr) < 0)
		{
			printf("\nERROR: os_ioctl error\n");
		}
		strcat(message, " IPv4 address - ");
		strcat(message, inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr));
			    
		if (os_ioctl(fd, SIOCGIFNETMASK, &ifr) < 0)
		{
			printf("\nERROR: os_ioctl error\n");
		}
		strcat(message, ", netmask - ");
		strcat(message, inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr));
		strcat(message, "\n");
			    
		os_close(fd);
	}
	else
	{
		strcat(message, " Interface is down \n");
	}
	
	return message;
}

//function using to get network interfaces MAC address

char *get_mac_address(char iface[])
{
	
	int fd, i;
	struct ifreq ifr;
	char *message = (char*)malloc(1000 * sizeof(char));
	char buff[5];
	
	strcpy(message, iface);
		     
	fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	
	if(fd == -1)
	{
		printf("\nERROR: Cannot create socket\n");
	}
		 
	strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);
		     
	if(os_ioctl(fd, SIOCGIFHWADDR, &ifr) < 0)
	{
		printf("\nERROR: os_ioctl error\n");
	}
	strcat(message, " MAC address - ");
	for (i = 0; i < 6; ++i)
	{
		sprintf(buff, "%02x", (unsigned char) ifr.ifr_addr.sa_data[i]);
		strcat(message, buff);
		if(i < 5)
		{
			strcat(message, ":");
		}
		buff[0] = 0;
	}
	strcat(message, "\n");
		    
	os_close(fd);
	return message;
}

//function using to get network interfaces IPv6 address

char *get_ipv6(char iface[])
{
	struct ifaddrs *ifa = NULL, *entry_ptr = NULL;
	void *addr_ptrs = NULL;
	int rc = 0;
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, iface);
	char buff[INET6_ADDRSTRLEN];
	rc = getifaddrs(&ifa);
	
	if (rc==0) 
	{
		for(entry_ptr = ifa; entry_ptr != NULL; entry_ptr = entry_ptr->ifa_next) 
		{
		    if(entry_ptr->ifa_addr->sa_data == NULL)
		    {
		    	continue;
		    }
		    if(entry_ptr->ifa_addr->sa_family == AF_INET6)
		    {
		    	addr_ptrs = &((struct sockaddr_in6 *)entry_ptr->ifa_addr)->sin6_addr;
		    }
		    else
		    {
		    	continue;
		    }
		    char *name = entry_ptr->ifa_name;
		    const char *inet_ptr = inet_ntop(entry_ptr->ifa_addr->sa_family, addr_ptrs, buff, sizeof(buff));
		    if(inet_ptr != NULL && strcmp(name,iface) == 0)
		    {
		    	strcat(message, " IPv6 address - ");
		    	strcat(message, inet_ptr);
		    	strcat(message, "\n");
		    }
	    }
	}
	else
	{
		printf("\nERROR: Cannot execute reference\n");
	}
	
	freeifaddrs(ifa);
	return message;
}

//function using to get network interfaces Up/down status

char *get_up_down_status(int fd, char iface[])
{
	char *message = (char*)malloc(1000 * sizeof(char));
	strcpy(message, iface);
	struct ifreq ifr;
	
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	if(os_ioctl(fd, SIOCGIFFLAGS, &ifr) == -1)
	{
		printf("\nERROR: os_ioctl error\n");
	}
	
	if(ifr.ifr_flags & IFF_UP)
	{
		strcpy(message, " status - up\n");;
	}
	else
	{
		strcpy(message, " status - down\n");
	}
	
	return message;
}

//function using to set network interfaces details

int set_interfaces_details(int fd, char *msg)
{
	char message[1000];
	strcpy(message,"");
	regex_t reg;
	int reti;
	if(msg == NULL)
	{
		printf("\nERROR: Cannot get message\n");
		return 1;
	}
	
	int i, k, count = 0;
	
	for (k = 0; k < strlen(msg); k++)
	{
		if(msg[k] == '-')
		{
			count++;
		}
	}
	
	char *interfaces[count + 1];
	interfaces[0] = strtok (msg, "-");
	
	for (i = 1; i < count + 1; i++)
	{
		interfaces[i] = strtok (NULL, "-");
	}
	if(if_interface_exist(fd, interfaces[1]) == 0)
	{
		if (count == 2)
		{
			reti = regcomp(&reg, "^([0-f]{2}:){5}[0-f]{2}$", REG_EXTENDED);
			if(reti)
			{
				printf("\nERROR: Could not compile regex\n");
				regfree(&reg);
				return 1;
			}
			reti = regexec(&reg, interfaces[2], 0, NULL, 0);
			if(reti == REG_NOMATCH)
			{
				printf("\nERROR: Wrong MAC address\n");
				regfree(&reg);
				return 1;
			}
			regfree(&reg);
			if(set_mac_address(fd, interfaces[1], interfaces[2]) != 0)
			{
				printf("\nERROR: Cannot set MAC address\n");
				strcat(message, "Cannot set data.\n");
				if (send_msg(fd, message, strlen(message)) != 0)
				{
					printf("\nERROR: Cannot send message\n");
					return 1;
				}
				return 1;
			}
		}
		else if (count == 3)
		{
			reti = regcomp(&reg, 
			        "^([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
			         "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
			         "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
			         "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$", REG_EXTENDED);
			if(reti)
			{
				printf("\nERROR: Could not compile regex\n");
				regfree(&reg);
				return 1;
			}
			reti = regexec(&reg, interfaces[2], 0, NULL, 0);
			if(reti == REG_NOMATCH)
			{
				printf("\nERROR: Wrong IPv4 address\n");
				regfree(&reg);
				return 1;
			}
			reti = regexec(&reg, interfaces[3], 0, NULL, 0);
			if(reti == REG_NOMATCH)
			{
				printf("\nERROR: Wrong netmask\n");
				regfree(&reg);
				return 1;
			}
			regfree(&reg);
			if(set_ip_address_and_netmask(fd, interfaces[1], interfaces[2], interfaces[3]) != 0)
			{
				printf("\nERROR: Cannot set IP address or Netmask\n");
				strcat(message, "Cannot set data.\n");
				if (send_msg(fd, message, strlen(message)) != 0)
				{
					printf("\nERROR: Cannot send message\n");
					return 1;
				}
				return 1;
			}
		}
	}
	else
	{
		strcat(message, "Interface ");
		strcat(message, interfaces[1]);
		strcat(message, " doesn't exist\n");
		if (send_msg(fd, message, strlen(message)) != 0)
		{
			printf("\nERROR: Cannot send message\n");
			return 1;
		}
	}
		
	return 0;
}

//function using to set network interfaces MAC address

int set_mac_address(int fd, char *iface, char mac_char[])
{
	if(iface == NULL)
	{
		printf("\nERROR: Cannot get interface name\n");
		return 1;
	}
	
	struct ifreq ifr;
	struct sockaddr hwaddr = {};
	 
	sscanf(mac_char, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &hwaddr.sa_data[0], &hwaddr.sa_data[1], &hwaddr.sa_data[2],
	   		&hwaddr.sa_data[3], &hwaddr.sa_data[4], &hwaddr.sa_data[5]);
	hwaddr.sa_family = ARPHRD_ETHER;
	
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strcpy(ifr.ifr_name, iface);
	ifr.ifr_hwaddr = hwaddr;
	
	if (os_ioctl(fd, SIOCSIFHWADDR, &ifr) < 0)
	{
		printf("\nERROR: Cannot set MAC address\n");
		return 1;
	}
	
	if (send_msg(fd, "Set MAC address\n", 17) != 0)
	{
		printf("\nERROR: Cannot send message\n");
		return 1;
	}
	
	return 0;
}

//function using to set network interfaces IPv4 address and netmask

int set_ip_address_and_netmask(int fd, char *iface_name, char *ip_addr, char *gateway_addr)
{
	if(iface_name == NULL || ip_addr == NULL || gateway_addr == NULL)
	{
		printf("\nERROR: Cannot get complete data\n");
		return 1;
	}
	struct sockaddr_in ip;
	struct ifreq ifr;
	
	inet_pton(AF_INET, ip_addr, &ip.sin_addr);
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr = ip.sin_addr;
	strncpy(ifr.ifr_name, iface_name, IFNAMSIZ-1);
	
	if (os_ioctl(fd, SIOCSIFADDR, &ifr) < 0) 
	{
		fprintf(stderr, "\nERROR: Cannot set IP address\n");
		perror(ifr.ifr_name);
		return 1;
	} 
			
	inet_pton(AF_INET, gateway_addr, &ip.sin_addr);
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr = ip.sin_addr;
	strncpy(ifr.ifr_name, iface_name, IFNAMSIZ-1);
	
	if (os_ioctl(fd, SIOCSIFNETMASK, &ifr) < 0) 
	{
		fprintf(stderr, "\nERROR: Cannot set Netmask\n");
		perror(ifr.ifr_name);
		return 1;
	}
	  
	if (send_msg(fd, "Set IP address and Netmask\n", 29) != 0)
   	{
		printf("\nERROR: Cannot send message\n");
		return 1;
   	}
	  
	return 0;
}

//function using to check network interfaces existing

int if_interface_exist(int fd, char iface[])
{
	int i = 1;	
	struct ifreq ifr = {};
	ifr.ifr_addr.sa_family = AF_INET;
	for (; ; ++i) 
	{
		ifr.ifr_ifindex = i;
		if(os_ioctl(fd, SIOCGIFNAME, &ifr) == -1)
		{
			break;
		}
		else
		{
			if(strncmp(iface, ifr.ifr_name, sizeof(ifr.ifr_name)) == 0)
			{
				return 0;
			}
		}
	}
	
	return 1;
}

//function using to check network interfaces status

int if_status(int fd, char iface[])
{
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
	
	if(os_ioctl(fd, SIOCGIFFLAGS, &ifr) == -1)
	{
		return 1;
	}
	
	if(ifr.ifr_flags & IFF_UP)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

//function using to send message to client when is no behaviour for client message

int another_option(int fd)
{
	char message[100];
	
	strcpy(message,"");
	strcat(message,"No behaviour for this option.\n");
	
	if (send_msg(fd, message, strlen(message)) != 0)
	{
		printf("\nERROR: Cannot send message\n");
		return 1;
	}
	
	return 0;
}
