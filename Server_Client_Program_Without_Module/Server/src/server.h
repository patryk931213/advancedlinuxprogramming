#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include "os.h"

int run_server(int port, int cl_count, pthread_t t2);
int init_server(int port, int cl_count);
int handle_server(int cl_count);
int handle_client(struct epoll_event* ev);
int get_message(int fd, char *msg);
void remove_client(int fd);
void *input_stdin(void *ptr);
void interrupt();
