#include "server.h"
#include "network_fun.h"
#include "list.h"
#include <pthread.h>
#include <errno.h>

int i = 0, j = 0, clients = 0;

struct file_descriptors
{
	int epoll_fd;
	int srv_fd;
	int cli_fd;
};

struct file_descriptors fds;
struct epoll_event e;

int first[2];
node *list = NULL;
pthread_t t1;

//main function using to run server

int run_server(int port, int cl_count, pthread_t t2)
{
	t1 = t2;
	list = initiation(list);
	pipe(first);
	fds.epoll_fd = -1;
	fds.srv_fd = -1;
	struct epoll_event es[cl_count];

	if(init_server(port, cl_count) != 0)
	{
		return 1;
	}

	for(;;)
	{
		if ((i = os_epoll_wait(fds.epoll_fd, es, cl_count, -1)) < 0)
		{
			printf("\nERROR: Cannot wait for events\n");
			if(os_close(fds.epoll_fd) < 0)
			{
				printf("\nERROR: Cannot close epoll_fd\n");
				return 1;
			}
			fds.epoll_fd = -1;
			if(os_close(fds.srv_fd) < 0)
			{
				printf("\nERROR: Cannot close srv_fd\n");
				return 1;
			}
			fds.srv_fd = -1;
			return 1;
		}
		for (--i; i > -1; --i)
		{
			if (es[i].data.fd == first[0])
			{
				for(j = clients; j > -1; --j)
				{
					if(&es[j].events != 0)
					{
						if(clients > 2)
						{
							delete_descriptors(list, &es[j]);
						}
					}
				}
				delete_list(list);
				return 1;
			}
			if (es[i].data.fd == fds.srv_fd)
			{
				if(handle_server(cl_count) != 0)
				{
					return 1;
				}
			}
			else if(handle_client(&es[i]) != 0)
			{
				if (os_epoll_ctl(fds.epoll_fd, EPOLL_CTL_DEL, es[i].data.fd, &es[i]) < 0)
				{
					printf("\nERROR: Cannot delete server socket from epoll\n");
					return 1;
				}
				if(os_close(es[i].data.fd) < 0)
				{
					printf("\nERROR: Cannot close es[%d].data.fd", i);
					return 1;
				}
				es[i].data.fd = -1;
			}
		}
	}

	return 0;
}

//function using to initialize server parameters

int init_server(int port, int cl_count)
{
	struct sockaddr_in srv_addr;
	memset(&srv_addr, 0, sizeof(srv_addr));
	memset(&e, 0, sizeof(e));

	if ((fds.srv_fd = os_socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0)
	{
		printf("\nERROR: Cannot create socket\n");
		return 1;
	}

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	srv_addr.sin_port = htons(port);

	if (os_bind(fds.srv_fd, (struct sockaddr*) &srv_addr, sizeof(srv_addr)) < 0)
	{
		printf("\nERROR: Cannot bind socket\n");
		if(os_close(fds.srv_fd) < 0)
		{
			printf("\nERROR: Cannot close srv_fd\n");
			return 1;
		}
		fds.srv_fd = -1;
		return 1;
	}

	if (os_listen(fds.srv_fd, 1) < 0) 
	{
		printf("\nERROR: Cannot listen\n");
		if(os_close(fds.srv_fd) < 0)
		{
			printf("\nERROR: Cannot close srv_fd\n");
			return 1;
		}
		fds.srv_fd = -1;
		return 1;
	}

	if ((fds.epoll_fd = os_epoll_create(cl_count)) < 0)
	{
		printf("\nERROR: Cannot create epoll\n");
		if(os_close(fds.srv_fd) < 0)
		{
			printf("\nERROR: Cannot close srv_fd\n");
			return 1;
		}
		fds.srv_fd = -1;
		return 1;
	}
	
	e.events = EPOLLIN;
	e.data.fd = first[0];
	os_epoll_ctl(fds.epoll_fd, EPOLL_CTL_ADD, first[0], &e);
	list = add_element(list, e.data.fd);
	clients++;

	e.data.fd = fds.srv_fd;

	if (os_epoll_ctl(fds.epoll_fd, EPOLL_CTL_ADD, fds.srv_fd, &e) < 0)
	{
		printf("\nERROR: Cannot add server socket to epoll\n");
		if(os_close(fds.epoll_fd) < 0)
		{
			printf("\nERROR: Cannot close epoll_fd\n");
			return 1;
		}
		fds.epoll_fd = -1;
		if(os_close(fds.srv_fd) < 0)
		{
			printf("\nERROR: Cannot close srv_fd\n");
			return 1;
		}
		fds.srv_fd = -1;
		return 1;
	}
	list = add_element(list, e.data.fd);
	clients++;

	return 0;
}

//function using to handle clients

int handle_client(struct epoll_event* ev)
{
	int choice;
	size_t len = 0;
	char *buff = 0;
	
	if ((ev->events & EPOLLERR) || (ev->events & EPOLLRDHUP) || (ev->events & EPOLLHUP))
	{
		printf("\nERROR: Bad event\n");
		remove_client(ev->data.fd);
		list = delete_element(list, ev->data.fd);
		clients--;
	}
	else if (ev->events & EPOLLIN)
	{
		if (os_len_read(ev->data.fd, &len, sizeof(size_t)) == -1)
		{
			printf("\nERROR: Cannot read length\n");
			printf("\n\n\nERROR: %s\n\n\n", strerror(errno));
			return 1;
		}
		buff = (char*)malloc((len+1) * sizeof(char));
		if (os_read(ev->data.fd, buff, len) == -1)
		{
			printf("\nERROR: Cannot read message\n");
		}
		buff[len] = 0;
		if (len < 1)
		{
			free(buff);
			return 1;
		}
		//printf("\n\nReceive from client (fd - %d):\n\n%s", ev->data.fd, buff);
		choice = get_message(ev->data.fd, buff);
		if(choice == 1)
		{
			free(buff);
			return 1;
		}
		else if (choice == 2)
		{
			remove_client(ev->data.fd);
			list = delete_element(list, ev->data.fd);
			clients--;
		}
		free(buff);
		len = 0;
	}

	return 0;
}

//function using to handle server

int handle_server(int cl_count)
{
	fds.cli_fd = -1;
	struct sockaddr_in cli_addr;
	socklen_t cli_addr_len;
	memset(&cli_addr, 1, sizeof(cli_addr));
	memset(&cli_addr_len, 0, sizeof(cli_addr_len));

	if ((fds.cli_fd = os_accept(fds.srv_fd, (struct sockaddr*) &cli_addr, &cli_addr_len)) < 0)
	{
		printf("\nERROR: Cannot accept client\n");
		if(os_close(fds.cli_fd) < 0)
		{
			printf("\nERROR: Cannot close cli_fd\n");
			return 1;
		}
		fds.cli_fd = -1;
		if(os_close(fds.epoll_fd) < 0)
		{
			printf("\nERROR: Cannot close epoll_fd\n");
			return 1;
		}
		fds.epoll_fd = -1;
		if(os_close(fds.srv_fd) < 0)
		{
			printf("\nERROR: Cannot close srv_fd\n");
			return 1;
		}
		fds.srv_fd = -1;
		return 1;
	}

	if (clients < cl_count + 2)
	{
		e.data.fd = fds.cli_fd;

		if (os_epoll_ctl(fds.epoll_fd, EPOLL_CTL_ADD, fds.cli_fd, &e) < 0)
		{
			printf("\nERROR: Cannot accept client\n");
			if(os_close(fds.cli_fd) < 0)
			{
				printf("\nERROR: Cannot close cli_fd\n");
				return 1;
			}
			fds.cli_fd = -1;
			if(os_close(fds.epoll_fd) < 0)
			{
				printf("\nERROR: Cannot close epoll_fd\n");
				return 1;
			}
			fds.epoll_fd = -1;
			if(os_close(fds.srv_fd) < 0)
			{
				printf("\nERROR: Cannot close srv_fd\n");
				return 1;
			}
			fds.srv_fd = -1;
			return 1;
		}
		
		list = add_element(list, e.data.fd);
		clients++;
	}
	else
	{
		printf("Cannot add more clients, file descriptor will be close\n");
		if(os_close(fds.cli_fd) < 0)
		{
			printf("\nERROR: Cannot close cli_fd\n");
			return 1;
		}
		fds.cli_fd = -1;
	}

	return 0;
}

//function using to handle messages

int get_message(int fd, char *msg)
{
	if(msg == NULL)
	{
		printf("\nERROR: Cannot get message\n");
		return 1;
	}
	switch(msg[0])
	{
		case '1':
			if(get_interfaces_list(fd) != 0)
			{
				printf("\nERROR: Cannot get interfaces list\n");
				return 1;
			}
			break;
			
		case '2':
			if(get_interfaces_details(fd, msg) != 0)
			{
				printf("\nERROR: Cannot get interfaces details\n");
				return 1;
			}
			break;
					
		case '3':
			if(set_interfaces_details(fd, msg) != 0)
			{
				printf("\nERROR: Cannot set interfaces details\n");
				return 1;
			}
			break;
			
		case '0':
			if(another_option(fd) != 0)
			{
				printf("\nERROR: Cannot get message\n");
				return 1;
			}
			break;
			
		case 'q':
			return 2;
			break;
			
		default:
			break;
			
	}
	return 0;
}

//function using to remove clients from epoll

void remove_client(int fd)
{	
	close(fd);
	e.data.fd = fd;
	os_epoll_ctl(fds.epoll_fd, EPOLL_CTL_DEL, fd, &e);
}

//function using to read from stdin

void *input_stdin(void *ptr)
{
	while(1)
	{
		char str[4];
		int result;
		result = read(STDIN_FILENO, str, 4);
		
		if(result < 0)
		{
			printf("\nERROR: Cannot read\n");
		}
		
		if(str[0] == 'x')
		{
			write(first[1], str, result);
			pthread_exit(&t1);
		}
	}
}
