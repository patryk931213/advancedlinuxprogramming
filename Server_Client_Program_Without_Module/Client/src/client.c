#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netdb.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

int send_msg(int fd, const char* msg, size_t len);
int parse_ip(char ip[16]);
int check_options(char *text);

int main(int argc, char* argv[])
{
	int choice;
	int port = 5555, sockfd, more_get = 0, enter = 0, end = 0, toSend = 0;
	char detal, kolejny,opcja=0;
	char ip[20];
	struct sockaddr_in serv_addr;
	char msg[256], temp[5], mac[100], ip4[100], mask[100], test_message[100];

	if (argc == 2)
	{
		if (atoi(argv[1]) != 0)
		{
			port = atoi(argv[1]);
		}
		else printf("Invalid port. Server will run with port: %d\n", port);
		strcpy(ip, "127.0.0.1");
	}

	if (argc == 3)
	{
		if (atoi(argv[1]) != 0)
		{
			port = atoi(argv[1]);
		}
		else printf("Invalid port. Server will run with port: %d\n", port);

		if(parse_ip(argv[2])==0)
		{
			strcpy(ip, argv[2]);
		}
		else
		{
			printf("Invalid IP address, check IP address and try again\n");
			return 1;
		}
	}
	
	if (argc == 4) //option for test only
	{
		if (atoi(argv[1]) != 0)
		{
			port = atoi(argv[1]);
		}
		else printf("Invalid port. Server will run with port: %d\n", port);

		if(parse_ip(argv[2])==0)
		{
			strcpy(ip, argv[2]);
		}
		else
		{
			printf("Invalid IP address, check IP address and try again\n");
			return 1;
		}
		choice = check_options(argv[3]);
		if(choice == 0)
		{
			strcpy(msg, argv[3]);
		}
		else if(choice == 2)
		{
			strcpy(msg, "q");
		}
		else if(choice == 1)
		{
			strcpy(msg, "0");
		}
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0)
	{
		printf("Cannot open port\n");
		return 1;
	}

	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(ip);
	serv_addr.sin_port = htons(port);

	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("Cannot connect with server\n");
		return 1;
	}

	if(argc == 4)
	{
		if (send_msg(sockfd, msg, strlen(msg)) != 0)
		{
			printf("Cannot send message\n");
		}
		size_t length = 0;
		char *buff = 0;
		read(sockfd, &length, sizeof(size_t));
		buff = (char*)malloc((length+1) * sizeof(char));
		buff[length] = 0;
		if (read(sockfd, buff, length) < 0)
		{
			printf("Cannot send message\n");
			free(buff);
		}
		else
		{
			system("clear");
			printf("Message received:\n\n%s\n", buff);
			free(buff);
		}
	}
	else
	{
		while (end == 0)
		{
			enter = 0;
			while (enter == 0)
			{
				printf("Choose what you want to do:\n");
				printf("1 - Show available network interfaces\n");
				printf("2 - Show intefaces details\n");
				printf("3 - Change interface settings\n");
				printf("Q - Quit\n");
				printf("Option -> ");
				scanf("%c", &opcja); //48 - 57
				switch (opcja)
				{
				case '1':
					strcpy(msg, "1");
					if (send_msg(sockfd, msg, strlen(msg)) != 0)
					{
						printf("Cannot send message\n");
					}
					else
					{
						enter = 1;
					}
					break;
				case '2':
					toSend = 0;
					more_get = 0;
					strcpy(msg, "2");
					while (toSend == 0)
					{
						system("clear");
						printf("Type name of interface: ");
						scanf("%s", temp);
						while (more_get == 0)
						{
							system("clear");
							printf("What information you want to get:\n");
							printf("1 - Up/Down status\n");
							printf("2 - Mac address\n");
							printf("3 - IPv4 address and netmask\n");
							printf("4 - IPv6 address\n");
							printf("5 - All details\n");
							printf("B - Back\n");
							printf("\nOption -> ");
							scanf("%c", &detal);
							switch (detal)
							{
							case '1':
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-a");
								break;
							case '2':
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-b");
								break;
							case '3':
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-c");
								break;
							case '4':
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-d");
								break;
							case '5':
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-a");
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-b");
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-c");
								strcat(msg, "-");
								strcat(msg, temp);
								strcat(msg, "-d");
							case 'b':
							case 'B':
								more_get = 1;
								break;
							default:
								system("clear");;
								printf("You pick wrong option. Try again\n");
								break;
							}
						}
						int quest = 0;
						while (quest == 0)
						{
							system("clear");
							printf("S - Send message\n");
							printf("N - Get information about next interface\n");
							printf("\nOption -> ");
							scanf("%c", &kolejny);
							switch (kolejny)
							{
							case 's':
							case 'S':
								toSend = 1;
								quest = 1;
								msg[strlen(msg)] = 0;
								break;
							case 'n':
							case 'N':
								quest = 1;
								more_get=0;
								temp[0] = 0;
								break;
							default:
								system("clear");;
								printf("You pick wrong option. Try again\n");
								break;
							}
						}

					}
					if (send_msg(sockfd, msg, strlen(msg)) != 0)
					{
						printf("Cannot send message\n");
					}
					else
					{
						enter = 1;
					}
					break;
				case '3':
					toSend = 0;
					more_get = 0;
					strcpy(msg, "3-");
					while (toSend == 0)
					{
						system("clear");
						printf("Type name of interface that you want to change: ");
						scanf("%s", temp);
						strcat(msg, temp);
						while (more_get == 0)
						{
							system("clear");
							printf("Choose what you want to change:\n");
							printf("1 - MAC Address\n");
							printf("2 - IPv4 address and netmask\n");
							printf("\nOption -> ");
							scanf("%c", &detal);
							switch (detal)
							{
							case '1':
								system("clear");
								printf("Type MAC address: ");
								scanf("%s", mac);
								strcat(msg, "-");
								strcat(msg, mac);
								more_get = 1;
								toSend = 1;
								break;
							case '2':
								system("clear");
								printf("Type IPv4 address: ");
								scanf("%s", ip4);
								strcat(msg, "-");
								strcat(msg, ip4);
								printf("Type netmask: ");
								scanf("%s", mask);
								strcat(msg, "-");
								strcat(msg, mask);
								more_get = 1;
								toSend = 1;
								break;
							default:
								system("clear");;
								printf("You pick wrong option. Try again\n");
								break;
							}
						}
					}
					if (send_msg(sockfd, msg, strlen(msg)) != 0)
					{
						printf("Cannot send message\n");
					}
					else
					{
						enter = 1;
					}
					break;
				case '4':
					system("clear");
					printf("Message -> ");
					scanf("%s", test_message);
					choice = check_options(test_message);
					if(choice == 0)
					{
						strcpy(msg, test_message);
					}
					else if(choice == 2)
					{
						strcpy(msg, "q");
					}
					else if(choice == 1)
					{
						strcpy(msg, "0");
					}
					if (send_msg(sockfd, msg, strlen(msg)) != 0)
					{
						printf("Cannot send message\n");
					}
					else
					{
						enter = 1;
					}
					break;
				case 'Q':
				case 'q':
					if (send_msg(sockfd, "q", 1) != 0)
					{
						printf("Cannot send message\n");
					}
					close(sockfd);
					exit(0);
					break;
				default:
					printf("You pick wrong option. Try again.\n");
					opcja = 'a';
					break;

				}
				scanf("%c", &opcja);

			}
			size_t length = 0;
			char *buff = 0;
			read(sockfd, &length, sizeof(size_t));
			buff = (char*)malloc((length+1) * sizeof(char));
			buff[length] = 0;
			if (read(sockfd, buff, length) < 0)
			{
				printf("Cannot send message\n");
				free(buff);
			}
			else
			{
				system("clear");
				printf("Message received:\n\n%s\n", buff);
				free(buff);
			}
		}
	}
	close(sockfd);
	return 0;
}

//function using to send message to server

int send_msg(int fd, const char* msg, size_t len)
{

	if(msg == NULL)
	{
		printf("Cannot send message\n");
		return 1;
	}

	if (write(fd, &len, sizeof(size_t)) != sizeof(size_t))
	{
		printf("Error while sending message\n");
		return 1;
	}

	if (write(fd, msg, len) != len)
	{
		printf("Error while sending message\n");
		return 1;
	}

	return 0;
}

//function using to check ip address propriety

int parse_ip(char ip[16])
{
	char x[4];
	int i,j,k=0,l=0,m=0,n=0;
	for(j=0;j<4;j++)
	{
		x[j]=0;
	}
	for(i=0;i<16 && m!=4;i++)
	{
		if(ip[i]=='.' || ip[i]==0)
		{
			k++;
			if((atoi(x)==0 && ip[i-1]!='0') || (atoi(x)==0 && l!=1)) return 1;
			l=0;
			if(ip[i]=='.') 
			{
				n++;
			}
			if(atoi(x)<0 || atoi(x)>255 || k>4 || n>3)
			{
				return 1;
			}
			else
			{
				for(j=0;j<4;j++) 
				{
					x[j]=0;
				}
				m++;
			}
		}
		else
		{
			if(l<4)
			{
				x[l]=ip[i];
			}
			else
			{
				return 1;
			}
			l++;
		}
	}
	if(m==4) 
	{
		return 0;
	}
	return 0;
}

//functions using to check message propriety

int check_options(char *text)
{
	if(text[0] == '1')
	{
		return 0;
	}
	else if(text[0] == '2' || text[0] == '3')
	{
		if(text[1] == '-')
		{
			if(text[2] == '-')
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 1;
		}
	}
	else if(text[0] == 'q' || text[0] == 'Q')
	{
		return 2;
	}
	else
	{
		return 1;
	}
}
